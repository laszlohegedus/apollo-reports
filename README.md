# apollo-reports

Elixir library that enables sending reports to Apollo Engine from within Elixir, without relying on proxy solutions.