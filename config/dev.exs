use Mix.Config

config :apollo_reports, ApolloReports.Phase.Send,
  reporter: {:system, :string, "APOLLO_REPORTER", "Elixir.ApolloReports.Reporter.TestReporter"}
