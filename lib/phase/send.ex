defmodule ApolloReports.Phase.Send do
  use Absinthe.Phase

  def run(bp, _options \\ []) do
    reporter = Confex.fetch_env(:apollo_reports, ApolloReports.Phase.Send)
               |> case do
                 {:ok, reporter: reporter} ->
                   String.to_atom(reporter)
                 :error ->
                   raise RuntimeError, "No reporter specified."
               end

    bp.result
    |> maybe_get(:extensions)
    |> maybe_get(:tracing)
    |> case do
      map when map == %{} ->
        :ok
      trace_info ->
        apply(reporter, :send, [trace_info])
    end

    {:ok, bp}
  end

  defp maybe_get(map, field) do
    Map.get(map, field, %{})
  end
end
