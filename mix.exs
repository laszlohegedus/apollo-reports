defmodule ApolloReports.MixProject do
  use Mix.Project

  def project do
    [
      app: :apollo_reports,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:absinthe, "~> 1.4"},
      {:apollo_tracing, "~> 0.4.0"},
      {:confex, "~> 3.4.0"},
      {:google_protos, "~> 0.1"},
      {:protobuf, "~> 0.5.3"},
      {:timex, "~> 3.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
