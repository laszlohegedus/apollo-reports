defmodule ApolloReportsTest do
  use ExUnit.Case

  defmodule ApolloReportsTest.Schema do
    use Absinthe.Schema
    use ApolloTracing

    query do
      field :hello, non_null(:string) do
        resolve(fn _parent, _args, _info -> {:ok, "hello"} end)
      end
    end
  end

  test "Schema" do
    query = """
    query {
      hello
    }
    """

    run_query(query, ApolloReportsTest.Schema)
  end

  defp run_query(query, schema, options \\ []) do
    pipeline = ApolloReports.Pipeline.default(schema, options)

    case Absinthe.Pipeline.run(query, pipeline) do
      {:ok, %{result: result}, _} -> {:ok, result}
      {:error, err, _} -> {:ok, err}
    end
  end
end
